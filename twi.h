#ifndef TWI_H
#define TWI_H
# define BIT(n) (1 << n)
# include "combined.h"


void		TWI_read(uint8_t addr, int nbr_of_bits, uint8_t data[nbr_of_bits]);
void		TWI_write(uint8_t addr, int nbr_of_bits, uint8_t data[nbr_of_bits]);
void		TWI_init(void);
int			TWI_wait();
int 		TWI_start(uint8_t addr, int write_read);
void		TWI_stop(void);

#endif