#ifndef COMBINED_H
#define COMBINED_H

# define BIT(n) (1 << n)
# define MAX(x , y) x > y ? y : x
# define MIN(x , y) x < y ? y : x
# define PI 3.14159265359

# include <string.h>
# include <avr/io.h>
# include <avr/interrupt.h>
# include <util/delay.h>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include "uart.h"
# include "twi.h"
# include "display.h"

uint8_t img[1025];
char	print_buff[100];

enum {
	DISPLAY_ADDR =	0x3c,
	ACC_ADDR =		0x53,
	COMPASS_ADDR =	0x1e,
	GYRO_ADDR =		0x69,
	L_SENS_ADDR  =	0x23
};

void	init_pwm(void);
void	init_int3(void);
void	init_timer();
void	setup_dimmer(void);
void	ft_dimmer(void);

void	ft_light_sensor();

void	init_accelerometer(void);
float	ft_angle(int x, int y);
void	ft_read_accel(int resulting_forces[3]);
void	ft_draw_horizon(void);


void ft_init_gyro(void);
void ft_read_gyro(void);

void ft_init_compass(void);
void ft_read_compass(void);

#endif