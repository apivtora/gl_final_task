
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

#define NBR_OF_CLOCKS 1000

int main()
{
	int fd;
	char buff[100];
	float angle;
	clock_t t_first;
	clock_t t_second;

	t_second = clock();
	pid_t p_id;

	fd = open("/dev/ttyACM0", O_RDONLY);
	while (1)
	{
		if (read(fd, buff, 30) < 0)
			printf("didn't read data\n\r");
		sscanf(buff,"%s%f", buff, &angle);
		bzero(buff, sizeof(buff));
		t_first = clock();
		if ((t_first - t_second) / NBR_OF_CLOCKS > 1)
		{
			p_id = fork();
			if (!p_id)
			{
				printf("angle = %3.2f\n", angle);
				if (angle >= 315 || angle < 45)
					execl("/usr/bin/xrandr", "xrandr", "--output", "eDP1", "--rotate", "normal", NULL);
				else if (angle >=45 && angle < 135)
					execl("/usr/bin/xrandr","xrandr", "--output", "eDP1", "--rotate", "right", NULL);
				else if (angle >= 135 && angle < 225)
					execl("/usr/bin/xrandr","xrandr", "--output", "eDP1", "--rotate", "inverted", NULL);
				else if (angle >= 225 && angle < 315)
					execl("/usr/bin/xrandr","xrandr", "--output", "eDP1", "--rotate", "left", NULL);
			}
			else
				t_second = clock();
		}
	}
}
