#ifndef DISPLAY_H
#define DISPLAY_H

#include "combined.h"

typedef struct	s_dot
{
	int x;
	int y;
}				t_dot;

void	ft_init_display(void);
void 	ft_out_text(const char *text, int x, int y);
void	ft_clear_screen(void);
void	ft_draw_line(t_dot first, t_dot sec); 
void	ft_put_pixel(int x, int y);
void 	ft_out_letter(const uint8_t *letter, int x, int y);
t_dot	ft_add_dots(t_dot first, t_dot second);
t_dot	ft_turn_ox(t_dot my_dot, float angle);
t_dot	ft_turn_oz(t_dot my_dot, float angle);
void	ft_refresh_screen(void);

#endif