
#include "combined.h"

#define PI 3.14159265359
#define DATA_FLAG 0x40

#include "imgs.h"

enum {
	DISPLAY_OFF 			= 0xae,
	DISPLAY_ON 				= 0xaf,
	DISPLAY_SET_PAGE_ADDR   = 0xb0,
	DISPLAY_SET_LOW_COLUMN  = 0x10,
	DISPLAY_SET_HIGH_COLUMN = 0x10,
	DISPLAY_CLOCKDIV        = 0xd5,
	DISPLAY_MUXRATIO        = 0xa8,
	DISPLAY_CONTRAST        = 0x81,
	DISPLAY_INVERSION       = 0xa6,
	DISPLAY_PRECHARGEPERIOD = 0xd9,
	DISPLAY_CHARGE          = 0x8d,
	DISPLAY_ADDRESSING_MODE = 0x20
};

t_dot ft_add_dots(t_dot first, t_dot second)
{
	first.x = first.x + second.x;
	first.y = first.y + second.y;
	return(first);
}

t_dot ft_turn_ox(t_dot my_dot, float angle)
{
	int temp;
	temp = my_dot.x;
	my_dot.x = my_dot.x * cos(angle) + my_dot.y * sin(angle);
	my_dot.y = -temp * sin(angle) + my_dot.y * cos(angle);
	return(my_dot);
}

t_dot ft_turn_oz(t_dot my_dot, float angle)
{
	int temp;
	temp = my_dot.x;
	my_dot.x = my_dot.x * cos(angle) - my_dot.y * sin(angle);
	my_dot.y = temp * sin(angle) + my_dot.y * cos(angle);
	return(my_dot);
}

void ft_clear_screen(void)
{
	img[0] = DATA_FLAG;
	memset(img + 1, 0x0, 1024);
}

void ft_put_pixel(int x, int y)
{
	img[(y / 8) * 128 + x + 1] |= (1 << (y % 8));
}

void ft_init_display(void)
{
	cli();
	TWI_write(DISPLAY_ADDR, 7, (uint8_t[7]){DISPLAY_OFF, DISPLAY_ADDRESSING_MODE,
								0x0,DISPLAY_INVERSION, DISPLAY_CHARGE,
								0x14, DISPLAY_ON});
	TWI_write(DISPLAY_ADDR, 4, (uint8_t[4]){DISPLAY_OFF, DISPLAY_ADDRESSING_MODE, 0x0, 0x0});
	print_polling("Display initialized\n\r");
	sei();
}

void ft_out_letter(const uint8_t *letter, int x, int y)
{
	int i = 0;
	int j = 0;
	while(j < 10)
	{
		i = 0;
		while (i < 10)
		{
			if (letter[j * 10 + i])
				ft_put_pixel(i + x, j + y);
			i++;
		}
		j++;
	}
}

void ft_out_text(const char *text, int x, int y)
{
	int i = 0;

	while (text[i])
	{
		ft_out_letter(alpha[text[i] - 32], x + i * 12, y);
		i++;
	}
}

#define SCR_TOP 0
#define SCR_BOT 63
#define SCR_LFT	0
#define SCR_RGT	127

void ft_draw_line(t_dot first, t_dot sec) 
{ 
	int dx;
	int sx;
	int dy;
	int sy;
	int err;
	int e2;

	dx =  sec.x > first.x ? (sec.x - first.x) : (first.x - sec.x); 
	sx = first.x < sec.x ? 1 : -1;
	dy = sec.y > first.y ? (sec.y - first.y) : (first.y - sec.y);
	sy = first.y < sec.y ? 1 : -1; 
	err = (dx > dy ? dx : -(dy))/2;
	while (1)
	{
		ft_put_pixel(first.x, first.y);
		if ((first.x == sec.x && first.y == sec.y) || first.x== SCR_RGT || first.y== SCR_TOP || first.y == SCR_BOT || first.x == SCR_LFT)
			break;
		e2 = err;
		if (e2 > -(dx)) 
		{
			err = err - dy;
			first.x += sx;
		}
		if (e2 < dy)
		{
			err += dx;
			first.y += sy;
		}
	}
}

void ft_refresh_screen(void)
{
	TWI_write(DISPLAY_ADDR, sizeof(img),img);
}