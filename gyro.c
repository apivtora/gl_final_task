
#include "combined.h"

#define GYRO_ADDR 0x69

void ft_init_gyro(void)
{
	uint8_t init_data[] = {0x20, 0x0f};

	TWI_write(GYRO_ADDR, sizeof(init_data), init_data);
	print_polling("init gyro DONE");
}

/*
All "random" numbers in draw arcs, show ox and oy
are just suitable places for each symbol
arrow symbolse are put in the places of some symbols in
ASCII TABLE for simpler usability reasons:
[ = arrow_45
\ = arrow_135
] = arrow_225
^ = arrow_315
_ = arrow_90
` = arrow_270
*/

#define ARR_45 "["
#define ARR_135 "\\"
#define ARR_225 "]"
#define ARR_315 "^"
#define ARR_90 "_"
#define ARR_270 "`"

void ft_draw_arcs()
{
	int x;
	int y = -25;

	while (y < 25)
	{
		x = -24;
		while (x < -15)
		{
			if (x*x + y*y > 600 && x*x + y * y < 650)
				ft_put_pixel(x + 64, y + 32);
			x++;
		}
		y++;
	}
	y = -25;
		while (y < 25)
	{
		x = 16;
		while (x < 25)
		{
			if (x*x + y*y > 600 && x*x + y * y < 650)
				ft_put_pixel(x + 64, y + 32);
			x++;
		}
		y++;
	}
}



void ft_show_ox(int value)
{
	value /= 100;
	if (value < 0)
	{
		ft_out_text(ARR_135, 75, 8);
		ft_out_text(ARR_315, 43, 47);
		if (value < -10)
		{
			ft_out_text(ARR_135, 71, 5);
			ft_out_text(ARR_315, 47, 50);
		}
		if (value < -50 )
		{
			ft_out_text(ARR_135, 67, 2);
			ft_out_text(ARR_315, 51, 53);
		}
	}
	else if (value > 0)
	{
		ft_out_text(ARR_45, 44, 8);
		ft_out_text(ARR_225, 74, 47);
		if (value > 10)
		{
			ft_out_text(ARR_45, 48, 5);
			ft_out_text(ARR_225, 70, 50);
		}
		if (value > 50)
		{
			ft_out_text(ARR_45, 52, 2);
			ft_out_text(ARR_225, 66, 53);
		}
	}
	ft_draw_arcs();
}

void ft_show_oy(int value)
{
	value /= 100;
	if (value < 0)
	{
		ft_out_text(ARR_90, 60, 17);
		if (value < -10)
			ft_out_text(ARR_90, 60, 12);
		if (value < -50 )
			ft_out_text(ARR_90, 60, 7);
	}
	else if (value > 0)
	{
		ft_out_text(ARR_270, 60, 41);
		if (value > 10)
			ft_out_text(ARR_270, 60, 46);
		if (value > 50)
			ft_out_text(ARR_270, 60, 51);
	}
	ft_draw_line((t_dot){64, 23}, (t_dot){64, 44});
	ft_draw_line((t_dot){65, 23}, (t_dot){65, 44});
}

void ft_show_oz(int value)
{
	value /= 100;
	if (value > 0)
	{
		ft_out_text(">", 72, 28);
		if (value > 10)
			ft_out_text(">", 77, 28);
		if (value > 50)
			ft_out_text(">", 82, 28);
	}
	else if (value < 0)
	{
		ft_out_text("<", 48, 28);
		if (value <= -10 )
			ft_out_text("<", 43, 28);
		if (value <= -50 )
			ft_out_text("<", 37, 28);
	}
	ft_draw_line((t_dot){55, 32}, (t_dot){74, 32});
	ft_draw_line((t_dot){55, 33}, (t_dot){74, 33});
}

extern int mode;

void ft_read_gyro(void)
{
	while(mode == 3)
	{

		uint8_t data[6];

		int three_axis[3];
		ft_clear_screen();

		TWI_write(GYRO_ADDR, 1, (uint8_t[1]){0x28 | BIT(7)});
		TWI_read(GYRO_ADDR, 6, data);

		three_axis[0] = (data[1] << 8) | data[0];
		three_axis[1] = (data[3] << 8) | data[2]; 
		three_axis[2] = (data[5] << 8) | data[4]; 

		sprintf(print_buff, "gyro read is : x = %d y = %d z = %d \n\r", three_axis[0] / 100,three_axis[1] / 100 ,three_axis[2]/ 100);	
		print_polling(print_buff);

		ft_show_oz(three_axis[2]);
		ft_show_oy(three_axis[1]);
		ft_show_ox(three_axis[0]);
		
		ft_refresh_screen();
		_delay_ms(100);
	}
}