
/*
button for mode changing connected to dig pin 13 (I'm ising encoder SW)
*/

#include "combined.h"
#define CHANGE_DELAY 65536 - 30000
int mode = 0;
int ENABLE_CHANGE = 1;

#define TIMER_CMP_VALUE 4000

static void init_button_check_timer(void)
{
	TCCR3B = (1 << WGM32) | (1 << CS32) | (1 << CS30); 
	OCR3A = TIMER_CMP_VALUE;
}

void sutup_mode_interrupt()
{
 	PCICR = BIT(0);
 	DDRB &= ~BIT(PB6);
 	PORTB |= BIT(PB6);
 	PCMSK0 |= BIT(PCINT6);
}

void setup(void)
{
	init_uart();
	TWI_init();
	sutup_mode_interrupt();
	init_button_check_timer();	
}

void init_hw(void)
{
	init_pwm();
	ft_init_display();
	ft_init_gyro();
	init_accelerometer();
	ft_init_compass();	
}

#define DISPLAY_ADDR 0x3c

void ft_show_mode(void)
{
	ft_clear_screen();
	sprintf(print_buff, "MODE %d", mode + 1);
	ft_out_text(print_buff, 30, 20);
	ft_refresh_screen();
	_delay_ms(1000);

	ft_clear_screen();
	switch (mode)
	{
		case 0:
			sprintf(print_buff, "HORIZON");
			ft_out_text(print_buff, 20, 20);
			break;
		case 1:
			sprintf(print_buff, "LT DIMMER");
			ft_out_text(print_buff, 10, 20);
			break;
		case 2:
			sprintf(print_buff, "MAN DIMMER");
			ft_out_text(print_buff, 5, 20);
			break;
		case 3:
			sprintf(print_buff, "GYROSCOPE");
			ft_out_text(print_buff, 10, 20);
			break;
		case 4:
			sprintf(print_buff, "COMPASS");
			ft_out_text(print_buff, 20, 20);
			break;
		default:
			sprintf(print_buff, "ERROR%d", mode + 1);
			ft_out_text(print_buff, 20, 20);
	}
	ft_refresh_screen();
	_delay_ms(1000);
}

int main(void)
{
	setup();
	init_hw();
	while(1)
	{
		ft_show_mode();
		switch(mode)
		{
			case 0:
				ft_draw_horizon();
				break;
			case 1:
				ft_light_sensor();
				break;
			case 2:
				ft_dimmer();
				break;
			case 3:
				ft_read_gyro();
				break;
			case 4:
				ft_read_compass();
				break;
			default:
				print_polling("MODE error");
		}
	}

}

ISR(PCINT0_vect)
{
	TIMSK3 = 1 << OCIE3A;
}

ISR(TIMER3_COMPA_vect) 
{

	if (((PINB >> 6) & 1) == 0)
		{
		mode = (mode + 1) % 5;
		sprintf(print_buff, "mode is %d\n\r", mode);
		print_polling(print_buff);
		}
	TIMSK3 &= ~(1 << OCIE3A);
}