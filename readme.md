# GL Final #
This is final project of Global logic C embedded base camp  
The task was to connect Arduino Mega(using avr dude) with  
bunch of diveces - display, digital encoder, accelerometer,
gyroscope, light sensor, compas.
Also when Arduino is connected to PC running Linux after tilting  
device over 45 degrees PC screen flips to certain orientation (if  
your video card supports this)

### Devices info ###
Controller - Arduino Mega2560 (using Atmel Mega 2560)  
Display - 1" OLED SSD1306  
Accelerometer- ADXL345  
Gyroscope - L3G4200D  
Light sensor - BH1750FVI  
Compass - HMC58883L  


### Connection scheme ###
![exmaple 0](https://bytebucket.org/apivtora/gl_final_task/raw/861852be0f7db33c9753a18b45fb39c7fd98c35e/Result/WP_20170904_10_10_58_Pro.jpg)


### Result ###
Accelerometer:  
![exmaple 1](https://bytebucket.org/apivtora/gl_final_task/raw/861852be0f7db33c9753a18b45fb39c7fd98c35e/Result/ezgif.com-video-to-gif%20%281%29.gif)

Light sensor:  
![exmaple 2](https://bytebucket.org/apivtora/gl_final_task/raw/861852be0f7db33c9753a18b45fb39c7fd98c35e/Result/ezgif.com-video-to-gif%20%282%29.gif)  

Encoder:  
![exmaple 3](https://bytebucket.org/apivtora/gl_final_task/raw/861852be0f7db33c9753a18b45fb39c7fd98c35e/Result/ezgif.com-video-to-gif%20%283%29.gif)

Gyroscope:  
![exmaple 4](https://bytebucket.org/apivtora/gl_final_task/raw/861852be0f7db33c9753a18b45fb39c7fd98c35e/Result/ezgif.com-video-to-gif%20%284%29.gif)

Compass:  
![exmaple 5](https://bytebucket.org/apivtora/gl_final_task/raw/861852be0f7db33c9753a18b45fb39c7fd98c35e/Result/ezgif.com-video-to-gif%20%285%29.gif)