
#include "combined.h"

void ft_write_bh1750(void)
{
	TWI_write(L_SENS_ADDR, 1, (uint8_t[1]){0x10});
}

int ft_read_bh1750(void)
{
	char print_buff[100];
	uint8_t data[2];
	int lux;

	TWI_read(L_SENS_ADDR, sizeof(data), data);
	lux = (data[0] << 8) + data[1];	   
	sprintf(print_buff, "brightnes is  %dlux\n\r", lux);
	print_polling(print_buff);
	return (lux);
}
extern int mode;

#define MAX_BRT 255
#define SENSIVITY 20 // the lower value - the bigger sensivity

void ft_light_sensor()
{
	int brightness;

	ft_write_bh1750();
	while(mode == 1)
	{
		brightness = ft_read_bh1750();
		OCR0A = MAX( brightness / SENSIVITY, MAX_BRT);
		_delay_ms(100);
		ft_clear_screen();
		sprintf(print_buff, "BRIGHTNESS");
		ft_out_text(print_buff, 10, 10);
		sprintf(print_buff, "%6d LUX", brightness);
		ft_out_text(print_buff, 10, 30);
		ft_refresh_screen();
	}

}