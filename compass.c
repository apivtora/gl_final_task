
#include "combined.h"

void ft_init_compass(void)
{
	TWI_write(COMPASS_ADDR, 2, (uint8_t[2]){0x00, 0x70});
	TWI_write(COMPASS_ADDR, 2, (uint8_t[2]){0x01, 0x80});
	TWI_write(COMPASS_ADDR, 2, (uint8_t[2]){0x02, 0x00});
	print_polling("Initialazing compass DONE");
}
extern int mode;

static const t_dot SCR_CENTER = {64, 32};

void ft_draw_compass(float azimuth)
{

	ft_clear_screen();

	t_dot North = {31, 0};
	t_dot South = {-30, 0};
	t_dot West = {0, -16};
	t_dot East = {0, +16};
	North = ft_add_dots(ft_turn_ox(North, azimuth), SCR_CENTER);
	South = ft_add_dots(ft_turn_ox(South, azimuth), SCR_CENTER);
	West = ft_add_dots(ft_turn_ox(West, azimuth), SCR_CENTER);
	East = ft_add_dots(ft_turn_ox(East, azimuth), SCR_CENTER);

	ft_draw_line(West, North);
	ft_draw_line(North, East);
	ft_draw_line(East, South);
	ft_draw_line(South, West);
	ft_draw_line(West, East);
	ft_draw_line(SCR_CENTER, North);
	ft_refresh_screen();
}

/*
those offset values are manually measured to match current
place magnetic fields, they can differ in other locations
*/
#define Y_OFFSET 166  
#define Z_OFFSET 87

void ft_read_compass(void)
{
	uint8_t data[6];
	int mag_data[3];
	float f_y;
	float f_z;

	float azimuth;

	while(mode == 4)
	{
		TWI_read(COMPASS_ADDR, sizeof(data), data);
		TWI_write(COMPASS_ADDR, 1, (uint8_t[1]){0x03});
		mag_data[0] = data[0] << 8 | data[1];
		mag_data[1] = data[2] << 8 | data[3];
		mag_data[2] = data[4] << 8 | data[5];
		f_y = MIN(MAX((float)(mag_data[1] + Y_OFFSET) / 101, 1), -1);
		f_z = MIN(MAX((float)(mag_data[2] + Z_OFFSET) / 101, 1), -1);
		azimuth = (atan(f_y / f_z)); 

		 if (f_z > 0 && f_y < 0) 
		 	azimuth += 2 * PI;
		 else if (f_z < 0)
		 	azimuth += PI;
		sprintf(print_buff, "compass data = %d %d %d azimuth = %d\n\r", mag_data[0], mag_data[1], mag_data[2], (int)((azimuth * 180) / PI));
		print_polling(print_buff);
		ft_draw_compass(azimuth);
	}
}