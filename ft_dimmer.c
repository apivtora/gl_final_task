
#include "combined.h"

/*
CLK connected to dig pin17
DT connected to to dig pin 18
Diode connected to dig pin 13
*/

uint8_t brightness;
extern int ENABLE_CHANGE;

void init_pwm(void)
{
    TCCR0A = BIT(COM0A1) | BIT(COM0B1) | BIT(WGM01) | BIT(WGM00);
    TCCR0B = BIT(CS00);
    DDRB |= BIT(PB7);
}

void init_int3(void)
{
		// EICRA = 0;
EICRA = BIT(ISC31); // setting up rising front trigger
EIMSK |= BIT(3);
DDRD &= ~BIT(PD3);
PORTD |= BIT(PD3);
}

void init_timer()
{
  TCCR1A = 0;
  TCCR1B = BIT(CS11); // using 256 prescaler
}

void setup_dimmer(void)
{
	cli();
	init_int3();
	init_timer();
	DDRD = ~(PH0); // digital pin 17
	sei();
}

#define CHANGE_DELAY 65536 - 30000
#define MAX_BRT 255
#define MIN_BRT 0
ISR(INT3_vect)
{
	if (ENABLE_CHANGE)
	{
	 	if ((PINH & 1) == 0)
		{
			print_polling("  ++\n\r");
			brightness = MAX(brightness + 16, MAX_BRT);
		}
		else
		{
			print_polling("--  \n\r");
			brightness = MIN(brightness - 16, MIN_BRT);
		}
		ENABLE_CHANGE = 0;
	 	TIMSK1 = BIT(TOIE1);
	 	TCNT1 = CHANGE_DELAY;
	}
}

ISR(TIMER1_OVF_vect)
{
	ENABLE_CHANGE = 1;
	TIMSK1 &= ~BIT(TOIE1);
}

extern int mode;

void ft_dimmer(void)
{
	setup_dimmer();
	while(mode == 2)
	{
		OCR0A = brightness;
		ft_clear_screen();
		sprintf(print_buff, "BRIGHTNESS");
		ft_out_text(print_buff, 10, 10);
		sprintf(print_buff, "%3d%%", (brightness * 100) / 255);
		ft_out_text(print_buff, 40, 30);
		ft_refresh_screen();
	} 
}