
#include "twi.h"

/*
Using dig pin 20 - as SDA and 21 as SCL
*/

enum TWI_STATUS {
    TWI_M_START = 0x08,
    TWI_M_START_REPEAT = 0x10,
    TWI_M_ARBITRATION_LOST = 0x38,

    TWI_M_SLAW_ACK = 0x18,
    TWI_M_SLAW_NACK = 0x20,
    TWI_M_WDATA_ACK = 0x28,
    TWI_M_WDATA_NACK = 0x30,

    TWI_M_SLAR_ACK = 0x40,
    TWI_M_SLAR_NACK = 0x48,
    TWI_M_RDATA_ACK = 0x50,
    TWI_M_RDATA_NACK = 0x58,
};

enum TWI_ERROR_STATUS {
    TWI_OK,
    TWI_ERROR,
    TWI_FATAL,
    TWI_UNKNOWN
};

static const char* twi_error_strings[] = {
    [ 0 ] = "Unknown status",
    [ TWI_M_START ]            = "START condition has been transmitted",
    [ TWI_M_START_REPEAT ]     = "A repeated START condition has been transmitted",
    [ TWI_M_ARBITRATION_LOST ] = "Arbitration lost",
    [ TWI_M_SLAW_ACK ]         = "SLA+W has been transmitted, got ACK",
    [ TWI_M_SLAW_NACK ]        = "SLA+W has been transmitted, got NACK",
    [ TWI_M_WDATA_ACK ]        = "Data has been transmitted, got ACK",
    [ TWI_M_WDATA_NACK ]       = "Data has been transmitted, got NACK",
    [ TWI_M_SLAR_ACK ]         = "SLA+R has been transmitted, got ACK",
    [ TWI_M_SLAR_NACK ]        = "SLA+R has been transmitted, got NACK",
    [ TWI_M_RDATA_ACK ]        = "Data has been received, sent ACK",
    [ TWI_M_RDATA_NACK ]       = "Data has been received, sent NACK",
};

char *TWI_status(char *status_str, int nbr)
{
    sprintf(status_str,"%s\n\r", twi_error_strings[nbr]);
    return (status_str);
}

void TWI_init(void)
{
    TWSR = 0x00;
    TWBR = 0x0C;
    TWCR = (1<<TWEN);
}

int TWI_check_status()
{
    enum TWI_ERROR_STATUS err = TWI_OK;

    switch (TWSR) {
        case TWI_M_SLAW_NACK:
        case TWI_M_SLAR_NACK:
        case TWI_M_WDATA_NACK:
        case TWI_M_START_REPEAT:
            err = TWI_ERROR;
            break;

        case TWI_M_START:
        case TWI_M_SLAW_ACK:
        case TWI_M_SLAR_ACK:
        case TWI_M_WDATA_ACK:
        case TWI_M_RDATA_ACK:
        case TWI_M_RDATA_NACK:
            err = TWI_OK;
            break;

        case TWI_M_ARBITRATION_LOST:
            err = TWI_FATAL;
            break;

        default:
            err = TWI_FATAL;
            break;
    }
    return err;
}

int TWI_wait()
{
	while (!(TWCR & (1 << TWINT)));
    return(TWI_check_status());
}

#define WRITE 0
#define READ 1

int TWI_start(uint8_t addr, int write_read)
{
    int err;
    TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
    err = TWI_wait();
    if (!err)
    {
        TWDR = (addr << 1) | write_read;
        TWCR = (1<<TWINT)|(1<<TWEN);
        err = TWI_wait();
    }
    return(err);
}
//send stop signal
void TWI_stop(void)
{
    TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
}

void TWI_write(uint8_t addr, int nbr_of_bits, uint8_t data[nbr_of_bits])
{
    int err;
    int i = 0;
    char buff[100];

    err = TWI_start(addr, WRITE);
    while(!err && i < nbr_of_bits)
    {
        TWDR = data[i];
        TWCR = (1<<TWINT)|(1<<TWEN);
        err = TWI_wait();
        i++;
    }
    if (err)
        print_polling(TWI_status(buff, TWSR));
    TWI_stop();
}

void TWI_read(uint8_t addr, int nbr_of_bits, uint8_t data[nbr_of_bits])
{
    int err;
    int i = 0;
    char buff[100];

    err = TWI_start(addr, READ);
    while(!err && i < nbr_of_bits)
    {
        if (i < nbr_of_bits - 1)
            TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
        else
            TWCR = (1<<TWINT)|(1<<TWEN);
        err = TWI_wait();
        data[i] = TWDR;
        i++;
    }
    if (err)
        print_polling(TWI_status(buff, TWSR));
    TWI_stop();
}
