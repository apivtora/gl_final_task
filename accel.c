#include "combined.h"

extern int mode;
void init_accelerometer(void)
{
	uint8_t mode[2] = {0x2d, 0x08};
	TWI_write(ACC_ADDR, 2, mode);
}

void ft_read_accel(int resulting_forces[3])
{
	uint8_t data[6];
	TWI_write(ACC_ADDR, 1, (uint8_t[1]){0x32});
	TWI_read(ACC_ADDR, sizeof(data), data);

	resulting_forces[0] = (data[0] | (data[1] << 8)); 
	resulting_forces[1] = (data[2] | (data[3] << 8)); 
	resulting_forces[2] = (data[4] | (data[5] << 8)); 
}


#define ACC_G_VALUE 255

float ft_angle(int x, int y)
{
	float angle;
	float n_x;
	float n_y;

	n_x = (float)(MIN(MAX(x, ACC_G_VALUE), -ACC_G_VALUE)) / ACC_G_VALUE;
	n_y = (float)(MIN(MAX(y, ACC_G_VALUE), -ACC_G_VALUE)) / ACC_G_VALUE;
	angle = atan(n_x / n_y);

	if (n_y > 0 && n_x < 0) 
		angle += 2 * PI;
	else if (n_y < 0 || n_x < 0)
		angle += PI;
	return(angle); 
}


#define OFFSET 100

#define TIP_H 5
#define TIP_W 6

static const t_dot SCR_CENTER = {64, 32};

extern int mode;

void ft_draw_horizon(void)
{
    int data[3];
    float angle;

    t_dot tip_top;
    t_dot tip_bot;
    t_dot line_end;

    while(mode == 0)
    {
		ft_clear_screen();
    	ft_read_accel(data);
    	angle = ft_angle(data[1], data[2]);
    	sprintf(print_buff, "angle = %3.2f\n\r", (angle * 180) / PI);
    	print_polling(print_buff);

    	tip_top = ft_add_dots(ft_turn_oz((t_dot){0, -TIP_H}, angle), SCR_CENTER);
    	tip_bot = ft_add_dots(ft_turn_oz((t_dot){-TIP_W, 0}, angle), SCR_CENTER);
    	line_end = ft_add_dots(ft_turn_oz((t_dot){-OFFSET, 0}, angle), SCR_CENTER);

    	ft_draw_line(tip_top, tip_bot);
    	ft_draw_line(tip_bot, line_end);
    	
    	tip_bot = ft_add_dots(ft_turn_oz((t_dot){TIP_W, 0}, angle), SCR_CENTER);
    	line_end = ft_add_dots(ft_turn_oz((t_dot){OFFSET, 0}, angle), SCR_CENTER);

    	ft_draw_line(tip_top, tip_bot);
    	ft_draw_line(tip_bot, line_end);
		ft_refresh_screen();
	}
}