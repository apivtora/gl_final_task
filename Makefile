DEVICE := atmega2560
F_CPU  := 16000000

PROGRAMMER := wiring
PORT   := /dev/ttyACM0
SPEED  := 115200

CC      := avr-gcc
AVRDUDE := avrdude -v -p$(DEVICE) -c$(PROGRAMMER) -P$(PORT) -b$(SPEED) -D -V

CFLAGS  += -Wall -Os -DF_CPU=$(F_CPU) -mmcu=$(DEVICE) -lm -Wl,-u,vfprintf -lprintf_flt

OBJECTS := main.o uart.o ft_dimmer.o ft_light_sensor.o \
			twi.o ft_display.o accel.o gyro.o compass.o
TMPOUT  := main.elf
HEADERS  := combined.h uart.h twi.h display.h
OUT     := main.hex

all: $(OUT) $(ROT)

flash: $(OUT)
	$(AVRDUDE) -U flash:w:$^:i

bin:
	gcc -o rotate_screen rotate_screen.c

clean:
	-rm -f $(OUT) $(TMPOUT) $(OBJECTS)
re: clean all

$(OUT): $(OBJECTS) $(HEADERS)
	$(CC) $(CFLAGS) -o $(TMPOUT) $^
	avr-objcopy -j .text -j .data -O ihex $(TMPOUT) $@
	avr-size --format=avr --mcu=$(DEVICE) $@