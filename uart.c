#include "uart.h"

char *send_data_ptr;

void init_uart(void)
{
	const uint16_t ubrr = F_CPU / 8 / 115200 - 1;

	UBRR0H = ubrr >> 8;
	UBRR0L = ubrr & 0xff;
	UCSR0A |= BIT(U2X0);

	UCSR0B = BIT(TXEN0);
	UCSR0C = 3 << UCSZ00;
}

int print_polling(const char str[])
{
	size_t i = 0;
	const size_t N = strlen(str);

	cli();
	UCSR0B |= BIT(UDRIE0);
	for (i = 0; i < N; i++) {
		UDR0 = str[i];
		while (!(UCSR0A & (1 << UDRE0)));
	}
	UCSR0B &= ~BIT(UDRIE0);
	sei();

	return N;
}

// void ft_print_interr(char *str)
// {
// 	send_data_ptr = str;
// 	UCSR0B |= BIT(UDRIE0);
// }

// ISR(USART0_UDRE_vect)
// {
//     UDR0 = *send_data_ptr;
//     send_data_ptr++;	
//     if (*send_data_ptr == '\0')
//     {
// 	UCSR0B &= ~BIT(UDRIE0);
//     }
// }

